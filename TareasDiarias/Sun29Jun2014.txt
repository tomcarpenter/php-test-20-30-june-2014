>>Finished notes functionality today, and tested. One of the main hurdles today was getting
  variables passed through Twig into JavaScript on the page. Essentially, this is done the
  same way that one would present PHP variables to JavaScript, because Twig is a template
  language based upon PHP. So, do the following on the Twig page, producing a global variable
  for any subsequent JavaScript:

     var javaScriptVar = {{ projectId }};

>>Removed the ACME demo bundle.

>>Tested in Firefox 30.0, Chrome 35.0, and IE9. The app. worked well in the first two browsers,
  but displayed quirky in IE9:

  --Had to reset advanced settings to get "http://localhost:8000/" to resolve.
    Note the final "/". The browser will open search results instead of navigating without that.

  --Hover styles on buttons don't work. Is there a special syntax for IE?

  --Text on "Search Notes" button wraps to two lines. Need to widen div?

  --The note search form is out of shape. It seems as if the title and description fields are
    aligned right (as they should be), but their widths are not anywhere near 300px!

    ..FIXED: Changed the "min-width" and "min-height" styles back to "width" and "height".

    ..Still, the alignment of labels with fields is off.


>>TO DO:
  
  >--Fix IE9 problems, retest in all browsers, and build results of test into document.

  --Finish document.

  --Get work up to BitBucket.