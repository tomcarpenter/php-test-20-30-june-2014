<?php

namespace Notes\NoteEditorBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'projects' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Notes.NoteEditorBundle.Model.map
 */
class ProjectsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Notes.NoteEditorBundle.Model.map.ProjectsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('projects');
        $this->setPhpName('Projects');
        $this->setClassname('Notes\\NoteEditorBundle\\Model\\Projects');
        $this->setPackage('src.Notes.NoteEditorBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 250, null);
        $this->getColumn('name', false)->setPrimaryString(true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Notes', 'Notes\\NoteEditorBundle\\Model\\Notes', RelationMap::ONE_TO_MANY, array('id' => 'project_id', ), 'CASCADE', null, 'Notess');
    } // buildRelations()

} // ProjectsTableMap
