<?php

namespace Notes\NoteEditorBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'notes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.Notes.NoteEditorBundle.Model.map
 */
class NotesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.Notes.NoteEditorBundle.Model.map.NotesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('notes');
        $this->setPhpName('Notes');
        $this->setClassname('Notes\\NoteEditorBundle\\Model\\Notes');
        $this->setPackage('src.Notes.NoteEditorBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('date', 'Date', 'DATE', false, null, null);
        $this->getColumn('date', false)->setPrimaryString(true);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 250, null);
        $this->getColumn('title', false)->setPrimaryString(true);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('project_id', 'ProjectId', 'INTEGER', 'projects', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Projects', 'Notes\\NoteEditorBundle\\Model\\Projects', RelationMap::MANY_TO_ONE, array('project_id' => 'id', ), 'CASCADE', null);
    } // buildRelations()

} // NotesTableMap
