/**   Document Ready JQuery for the projects browser template.
 * 
 */
$(function(){
    var projectNameCleared = false;
    
    //Menu functions:
    //---------------
    $(".home_link").on("click", function(){
      window.location.href = baseUrl;
    });

    $(".searchProjects_home_link").on("click", function(){
      window.location.href = baseUrl+searchProjectsRoute;
    });

    $(".searchProjects_link").on("click", function(){
      window.location.href = baseUrl+searchProjectsRoute;
    });
    
    //Editor functions:
    //-----------------
    /**   Clears messages and sometimes form-field values
     * when the user clicks on the response form.
     */
    $(document).on("click", function(){
       //Always clear the status message:
       $(".msg").text('');
       //Only clear the name field on the "Create Project" form 
       //if it hasn't been cleared already:
       if($(".proj_heading").text() == "Create Project" && projectNameCleared == false){
          $("#Project_name").val('');
          projectNameCleared = true;
       }
    });
    /**   Respond to clicking the "Create Project" button.
     */
    $(".createProject").on("click", function(){
        window.location.href = baseUrl+createProjectRoute;
    });    
    /**   Respond to clicking the "Search Notes" button.
     */
    $(".searchNotes").on("click", function(){
        var id = $(this).parent().attr("id");
        window.location.href = baseUrl+searchNotesRoute+id;
    });
    /**   Respond to clicking the "Add a Note" button.
     */
    $(".createNote").on("click", function(){
        var id = $(this).parent().attr("id");
        window.location.href = baseUrl+createNoteRoute+id;
    });
    /**   Respond to clicking the "Edit Project" button.
     */
    $(".editProject").on("click", function(){
        var id = $(this).parent().attr("id");
        window.location.href = baseUrl+editProjectRoute+id;
    });
    /**   Respond to clicking the "Delete Project" button.
     * This is an AJAX function that inserts its response
     * back into the search projects page.
     */
    $(".deleteProject").on("click", function(){
        var id = $(this).parent().attr("id");        
        var boolConfirm = confirm('Delete project, "'+$(this).siblings(".project").text()+
           '", and all related notes?');
        if(boolConfirm == true){
           $.ajax({
             url: baseUrl+deleteProjectRoute+id,
             context: document.body
           }).done(function(data){
              $(".msg").text(data);
              $(".msg").css("color", "blue");
              $("#"+id).remove();
           }).fail(function( jqXHR, textStatus ) {
              $(".msg").text("Deletion failed: "+textStatus);
              $(".msg").css("color", "red");
           });
        }
    });

});
