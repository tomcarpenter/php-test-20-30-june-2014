/**   Global variables for use throughout the application's JavaScript. 
 *
 */
var baseUrl = 'http://localhost:8000';

var searchProjectsRoute = '/search_projects_test';
var createProjectRoute = '/create_project_test';
var editProjectRoute = '/edit_project_test/';
var deleteProjectRoute = '/delete_project_test/';
var searchNotesRoute = '/search_notes_test/';
var createNoteRoute = '/create_note_test/';
var editNoteRoute = '/edit_note_test/';
var deleteNoteRoute = '/delete_note_test/';
