/**   Document Ready JQuery for the notes browser template.
 * 
 */
$(function(){
    //Menu functions:
    //---------------
    $(".home_link").on("click", function(){
      window.location.href = baseUrl;
    });

    $(".searchProjects_link").on("click", function(){
      window.location.href = baseUrl+searchProjectsRoute;
    });

    $(".searchNotes_link").on("click", function(){
      window.location.href = baseUrl+searchNotesRoute+projectId;
    });
    
    //Editor functions:
    //-----------------
    /**   Clears messages and sometimes form-field values
     * when the user clicks on the response form.
     */
    $(document).on("click", function(){
       //Only clear the name field on the "Create Project" form 
       //if it hasn't been cleared already:
       if($(".proj_heading").text() == "Create Project" && $(".msg").text() != ''){
          $("#Project_name").val('');
       }
       //Only clear the title and description fields on the "Create Note" 
       //form if they haven't been cleared already:
       if($(".note_heading").text().indexOf("Create Note") >= 0 && $(".msg").text() != ''){
          $("#Note_date_year").val('');
          $("#Note_date_month").val('');
          $("#Note_date_day").val('');
          $("#Note_title").val('');
          $("#Note_description").val('');
       }
       //Always clear the status message:
       $(".msg").text('');
    });
    /**   Respond to clicking the "Create Note" button.
     */
    $(".createNote").on("click", function(){
        window.location.href = baseUrl+createNoteRoute+projectId;
    });
     
    /**   Respond to clicking the "Edit Note" button.
     */
    $(".editNote").on("click", function(){
        var id = $(this).parent().attr("id");
        window.location.href = baseUrl+editNoteRoute+id;
    });
    /**   Respond to clicking the "Delete Note" button.
     * This is an AJAX function that inserts its response
     * back into the search projects page.
     */
    $(".deleteNote").on("click", function(){
        var id = $(this).parent().attr("id");        
        var boolConfirm = confirm('Delete note, "'+$(this).siblings(".note").text()+'"?');
        if(boolConfirm == true){
           $.ajax({
             url: baseUrl+deleteNoteRoute+id,
             context: document.body
           }).done(function(data){
              $(".msg").text(data);
              $(".msg").css("color", "blue");
              $("#"+id).remove();
           }).fail(function( jqXHR, textStatus ) {
              $(".msg").text("Deletion failed: "+textStatus);
              $(".msg").css("color", "red");
           });
        }
    });

});
