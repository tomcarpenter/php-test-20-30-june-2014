<?PHP

namespace Notes\NoteEditorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectType extends AbstractType
{
    private $type;

    function __construct($formType){
       $formType = trim($formType);
       if($formType != ''){
          $this->type = $formType;
       }else{
          $this->type = 'submit';
       }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('submit', 'submit', array('label' => $this->type));
    }

    public function getName()
    {
        return 'Project';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'required' => false,
            'data_class' => 'Notes\NoteEditorBundle\Model\Projects',
            'validation_groups' => array('Create', 'Edit')
        ));
    }
}

?>