<?PHP

namespace Notes\NoteEditorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NoteType extends AbstractType
{
    private $type;

    function __construct($formType){
       $formType = trim($formType);
       if($formType != ''){
          $this->type = $formType;
       }else{
          $this->type = 'submit';
       }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'date', array('empty_value' => ''))
            ->add('title')
            ->add('description')
            ->add('submit', 'submit', array('label' => $this->type));
    }

    public function getName()
    {
        return 'Note';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'required' => false,
            'data_class' => 'Notes\NoteEditorBundle\Model\Notes',
            'validation_groups' => array('Create', 'Edit')
        ));
    }
}

?>
