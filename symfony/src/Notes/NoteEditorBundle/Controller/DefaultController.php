<?php

namespace Notes\NoteEditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Notes\NoteEditorBundle\Model\Projects;
use Notes\NoteEditorBundle\Model\Notes;
use Notes\NoteEditorBundle\Form\ProjectType;
use Notes\NoteEditorBundle\Form\NoteType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
   /**   Attempts to delete a project from the "project" table having the "id" passed in.
    * Should this project have related notes, then they are deleted too. If successful, 
    * an acknowledgement of the records deleted is displayed.
    *
    * Parameters passed in:
    *
    *    $id -- (int) The primary key in "projects" of the record to edit.
    * 
    *    $request -- The http request object.
    */
    public function deleteProjectAction($id, Request $request){
       $project = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
          ->findPk($id);
       if(!$project) {
          throw $this->createNotFoundException(
            'No project found for id: '.$id
          );
       } 
       $notes = \Notes\NoteEditorBundle\Model\NotesQuery::create()
          ->filterByProjectId($id)
          ->find();
       $project->delete();

       $returnValue = new Response('Deleted an existing project, "'.
          $project->getname().'", and all related notes (count: '.$notes->count().').');
       
       return $returnValue;
    }
    
   /**   Attempts to delete a note from the "notes" table having the "id" passed in.
    * If successful, an acknowledgement of the record's deletion is displayed.
    *
    * Parameters passed in:
    *
    *    $id -- (int) The primary key in "notes" of the record to edit.
    * 
    *    $request -- The http request object.
    */
    public function deleteNoteAction($id, Request $request){
       $note = \Notes\NoteEditorBundle\Model\NotesQuery::create()
          ->findPk($id);
       if(!$note) {
          throw $this->createNotFoundException(
            'No note found for id: '.$id
          );
       } 
       $note->delete();

       $returnValue = new Response('Deleted an existing note, "'.$note->getTitle().'".');
       
       return $returnValue;
        
    }
    
   /**   Presents a project form, populated for the id value passed in, and offers
    * the user an opportunity to edit the record and repersist. Upon form submission, 
    * the form is first validated (see "\Notes\NoteEditorBundle\Resources\
    * config\validation.yml"), and if ok, updated in the "projects" table. 
    * If successful, an acknowledgement of the record's creation is displayed.
    *
    * Parameters passed in:
    *
    *    $id -- (int) The primary key in "projects" of the record to edit.
    * 
    *    $request -- The http request object.
    */
    public function editProjectAction($id, Request $request){
       $project = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
          ->findPk($id);
       if(!$project) {
          throw $this->createNotFoundException(
            'No project found for id: '.$id
          );
       }
       $form = $this->createForm(new ProjectType('Edit'), $project,
           array('validation_groups' => array('Edit')));     
       $form->handleRequest($request);

       $returnValue = $this->render('NotesNoteEditorBundle:Default:projectForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Edit Project'));
 
       if('POST' === $request->getMethod()){
            if ($form->isValid()){
                $project->save();

                $returnValue = $this->render('NotesNoteEditorBundle:Default:projectForm.html.twig', 
                   array('form' => $form->createView(), 'heading' => 'Edit Project',
                   'msg' => 'Edited an existing project, '.$project->getName().'.'));
            }
       }
       return $returnValue; 
    }
    
   /**   Presents a note form, populated for the id value passed in, and offers
    * the user an opportunity to edit the record and repersist. Upon form submission, 
    * the form is first validated (see "\Notes\NoteEditorBundle\Resources\
    * config\validation.yml"), and if ok, updated in the "notes" table. 
    * If successful, an acknowledgement of the record's creation is displayed.
    *
    * Parameters passed in:
    *
    *    $id -- (int) The primary key in "notes" of the record to edit.
    * 
    *    $request -- The http request object.
    */
    public function editNoteAction($id, Request $request){
       $note = \Notes\NoteEditorBundle\Model\NotesQuery::create()
          ->findPk($id);
       if(!$note) {
          throw $this->createNotFoundException(
            'No note found for id: '.$id
          );
       }
       $form = $this->createForm(new NoteType('Edit'), $note,
           array('validation_groups' => array('Edit')));     
       $form->handleRequest($request);

       //Get current project name:
       $project = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
          ->findPk($note->getProjectId());
       $projectName = '';
       if($project != null){
          $projectName = ' under "'.$project->getName().'"';         
       }

       $returnValue = $this->render('NotesNoteEditorBundle:Default:noteForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Edit Note'.$projectName,
          'projectId' => $note->getProjectId()));
 
       if('POST' === $request->getMethod()){
          if($form->isValid()){
             $note->save();

             $returnValue = $this->render('NotesNoteEditorBundle:Default:noteForm.html.twig', 
                array('form' => $form->createView(), 'heading' => 'Edit Note'.$projectName,
                'projectId' => $note->getProjectId(), 'msg' => 'Edited an existing note, "'.
                $note->getTitle().'"'));
          }
       }
       return $returnValue;  
    }
    
    /**   Presents a project form. Upon form submission, the form is first validated 
    * (see "\Notes\NoteEditorBundle\Resources\config\validation.yml"), and if ok, 
    * inserted into the "projects" table. If successful, an acknowledgement of the 
    * record's creation is displayed.
    *
    * Parameters passed in:
    * 
    *    $request -- The http request object.
    */
    public function createProjectAction(Request $request)
    {
       $project = new Projects();
       $form = $this->createForm(new ProjectType('Create'), $project,
           array('validation_groups' => array('Create')));           
       $form->handleRequest($request);

       $returnValue = $this->render('NotesNoteEditorBundle:Default:projectForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Create Project'));
 
       if('POST' === $request->getMethod()){
            if ($form->isValid()){
                $project->save();

                $returnValue = $this->render('NotesNoteEditorBundle:Default:projectForm.html.twig', 
                   array('form' => $form->createView(), 'heading' => 'Create Project', 
                   'msg' => 'Created a new project, "'.$project->getName().'".'));
            }
       }
       return $returnValue;
    }

    /**   Presents a note form, initialized for the passed-in project id and today's date.
    * Upon form submission, the form is first validated (see "\Notes\NoteEditorBundle
    * \Resources\config\validation.yml"), and if ok, inserted into the "notes" table.
    * If successful, an acknowledgement of the record's creation is displayed.
    *
    * Parameters passed in:
    *
    *    $projectId -- (int) A foreign key to the "projects" table.
    * 
    *    $request -- The http request object.
    */
    public function createNoteAction($projectId, Request $request)
    {
       $note = new Notes();
       $note->setDate(date('Y-m-d'));
       $note->setProjectId($projectId);
       $form = $this->createForm(new NoteType('Create'), $note,
           array('validation_groups' => array('Create')));     
       $form->handleRequest($request);

       //Get current project name:
       $project = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
          ->findPk($projectId);
       $projectName = '';
       if($project != null){
          $projectName = ' under "'.$project->getName().'"';         
       }
       $returnValue = $this->render('NotesNoteEditorBundle:Default:noteForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Create Note'.$projectName,
          'projectId' => $projectId));
 
       if('POST' === $request->getMethod()){
            if ($form->isValid()){
                $note->save();

                $returnValue = $this->render('NotesNoteEditorBundle:Default:noteForm.html.twig', 
                   array('form' => $form->createView(), 'heading' => 'Create Note',
                   'heading' => 'Create Note'.$projectName, 'projectId' => $projectId,
                   'msg' => 'Created a new note, "'.$note->getTitle().'".'));
            }
       }
       return $returnValue;
    }

    /**   Displays a search form for project names. Entering any part of a project name will
    * result in one or more full project names displayed. If nothing is entered, then no results
    * will be displayed. Searches are case-insensitive. In all cases, the search form is displayed 
    * on top.
    *
    * Parameter passed in:
    * 
    *    $request -- The http request object.
    */
    public function searchProjectsAction(Request $request)
    {
       $project = new Projects();
       $form = $this->createForm(new ProjectType('Search'), $project,
           array('validation_groups' => array('Search')));
       $form->handleRequest($request);

       $returnValue = $this->render('NotesNoteEditorBundle:Default:projectForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Search Projects'));

       $name = trim($project->getName()); 
       if($name != ''){
          $projects = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
             ->setIgnoreCase(true)
             ->filterByName('%'.$name.'%')
             ->orderByName()
             ->find();

          $returnValue = $this->render('NotesNoteEditorBundle:Default:projects.html.twig', 
             array('projects' => $projects, 'form' => $form->createView(), 
             'heading' => 'Search Projects'));
       }
       return $returnValue;
    }

    /**   Displays a search form for notes. Entering any part of the fields, "title", "description",
    * "date", will result in one or more full note records to be displayed. If nothing is entered, 
    * then no results will be displayed. Searches are case-insensitive. If a foreign key to the 
    * "projects" table is passed-in, then the search will be constrained to all notes under that
    * project. In all cases, the search form is displayed on top.
    *
    * Parameters passed in:
    *
    *    $projectId -- (int, defaults at 0) This optional parameter is a foreign key to the
    *       "projects" table. If entered, will limit the search to all notes under that project.
    * 
    *    $request -- The http request object.
    */
    public function searchNotesAction($projectId=0, Request $request)
    {
       $note = new Notes();
       $form = $this->createForm(new NoteType('Search'), $note,
           array('validation_groups' => array('Search')));
       $form->handleRequest($request);
       
       //Get current project name:
       $project = \Notes\NoteEditorBundle\Model\ProjectsQuery::create()
          ->findPk($projectId);
       $projectName = '';
       if($project != null){
          $projectName = ' under "'.$project->getName().'"';         
       }
       $returnValue = $this->render('NotesNoteEditorBundle:Default:noteForm.html.twig', 
          array('form' => $form->createView(), 'heading' => 'Search Notes'.$projectName,
          'projectId' => $projectId));

       $title = trim($note->getTitle());
       $description = trim($note->getDescription()); 
       $date = $note->getDate();
       if($title != '' || $description != '' || $date != '' ){
          $notes = \Notes\NoteEditorBundle\Model\NotesQuery::create()
             ->setIgnoreCase(true)
             ->_if($title != '')
               ->filterByTitle('%'.$title.'%')
             ->_endif()
             ->_if($description != '')
               ->filterByDescription('%'.$description.'%')
             ->_endif()
             ->_if($date != '')
               ->filterByDate($date)
             ->_endif()
             ->_if($projectId != 0)
               ->filterByProjectId($projectId)
             ->_endif()
             ->orderByDate()
             ->find();

          $returnValue = $this->render('NotesNoteEditorBundle:Default:notes.html.twig', 
             array('notes' => $notes, 'form' => $form->createView(), 
             'heading' => 'Search Notes'.$projectName, 'projectId' => $projectId));
       }
       return $returnValue;
    }

    public function indexAction()
    {
        return $this->render('NotesNoteEditorBundle:Default:index.html.twig');
    }
}
