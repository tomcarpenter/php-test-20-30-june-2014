Candidate Coding Challenge:

The end goal of this project is to create a simple Symfony2-based application with basic C.R.U.D. for one or more MySQL or MSSQL tables. We use this test project to assess your syntactical style, ability to follow directions, and creativity. While we’re primarily interested in your PHP skills, attention to application structure, user interface and design acumen, and adherence to coding standards are also considered. The subject of the application is up to you and is given no consideration in our assessment; feel free to pick any subject matter that interests you. Ask for additional guidance if any directions are unclear.

To Get Started


•	Install Symfony2 (version 2.4.~) using Composer **Done**

•	Install 2 vendors using Composer **Done**

- friendsofsymfony/jsroutingbundle **Done**

- propel/propelbundle **Done**

•	Create database interactivity using Propel ORM **Done**

•	Create database table(s) and populate with data. **Done**

Project Requirements

•	Using Git, branch (throughout development) and reintegrate project

•	Remove the Acme Demo Bundle **Done**

•	Create a bundle in YAML format **Done**

•	Build at least one controller with multiple actions **Done**

- Controller should execute C.R.U.D. on one or more or your tables **Done**

•	Create 2 or more views using Twig **Done**

•	Add CSS and JS to your views using Assetic **Done**
 
Extra Credit

•	Use Symfony2 forms and validation components **Done**

•	Utilize Symfony’s security component

•	Build a relational table with a foreign key assigned in your schema **Done**

•	AJAX something! **Done**

Deliverable

•	Push your code to a public repository - either Github or Bitbucket and let us know where we can get it **Done**

•	E-mail us the database **Done**

•	Give us an honest assessment of your application. What could be done better, what feature do you think is outstanding? If given more time, what else would you add? **Done**
 
**My project is a simple project/note management application. One can view, create, update, or delete project names, and notes under projects. In the first release, a line of text identifies each project. Notes, however, have dates, titles, and descriptions, where the note's title is a line of text, but its description can fit a long text field.**

**One can search through the database for projects by entering a project's entire name or any part of a name (which may result in more than one result). Similarly, all of a note's fields are searchable, and are joined by SQL-AND, if not empty. Empty fields are ignored. Searches are case-insensitive. A wildcard character * can be entered into a field to indicate that the field can match any characters. Wildcards are useful to display all project names, or all notes under a project.**

**The results of a project search orders projects alphabetically by their names. After a project has been selected, the results of a note search orders notes ascending by date.**

**Below, you will find a description of how I tested the application, problems I identified, and suggested improvements. To continue my Symfony education, I will begin to implement my suggestions.**

 
**Critique:**

•	I have tested this application only in development mode with the built-in server, that publishes to http://localhost/8000. (IE9 seems to require a final “/” to prevent the browser from searching on the address string.)

- So, if you intend to run this application on another server, then you must change my hardcoded base url, aside from any standard Symfony reconfigurations. My hardcoded url can be found in Notes/NoteEditorBundle/Resources/public/js/globalVars.js:
              		
var baseUrl = 'http://localhost:8000';

•	All forms, div buttons, and results browsers render well and function in Firefox 30.0, Chrome 35.0, and IE9. I have not tested the application in other browsers.

•	Although I installed the FOSJsRoutingBundle, my routes were simple enough to run straight from jQuery and AJAX, so I did not use the bundle.

•	In the present version, it would be possible to resubmit a form by navigating back to the form or by refreshing in the browser. This could be remedied by passing the current timestamp in a hidden field, which would be compared, on the server, with the last submitted timestamp for the form in session. If the current timestamp is greater than that in session, then the submitted data would be processed, and the current timestamp would then replace that saved in session. But, if the form’s timestamp were equal to that on the server, then the submitted data would not be processed, because the form was resubmitted by clicking back-arrow or refresh.

•	Currently, there is no partitioning of notes by user. Log-in security should be implemented to make this a multi-user application. 

•	Also, project and note locking should be implemented to make this a better multi-user application. I suggest making fields read-only (and color-coded as such) if another user is currently editing them. A user id field on a project or note could facilitate this, whereby another user could be informed of who is currently working the record.

- Logging in as a viewer (read-only) or as an editor, and only locking a record when editable fields are accessed by the user, could minimize periods of lock pandemonium.  Consequently, better read-only screens should be developed. (Currently, to view an entire note, one must enter the note’s editor.)

•	I could style the forms better than I had, and perhaps even implement a rich-text editor for notes (e.g.: the CKEditor). A rich-text editor could also enable the uploading of images with notes.

•	A way to instantly display all project names, and all notes under a project, would also be handy.

•	Also, displaying project create dates, and being able to sort by them, or by name, would be useful—as would being able to sort note search results on any field.

•	Rather than jumping between search views and editor views, AJAX and targeted divs could be utilized to keep as much information on one page as possible, thereby enabling quick cross-referencing and navigation between projects and notes.

•	I like my use of the same form (in Twig) for searching, editing, and creating records, and how one Twig file can be embedded within another (as I have done with the search forms and results browsers). Also, I like my controller actions that handle submissions of the search forms. I found that conditional logic can be placed within the Propel query, making it dynamic, on the basis of what fields have submitted data.

- I could improve on the notes search action by incorporating more advanced search options, like OR and parentheses, which would be selectable in the view.